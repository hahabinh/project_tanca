import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  Pressable,
  View,
  Image,
  TouchableOpacity,
} from 'react-native';
import React, {useState} from 'react';
import {Display} from '../utils';
import {Images} from '../constants';
import AsyncStorage from '@react-native-async-storage/async-storage';

const HomeModal = ({navigation}) => {
  const [modalVisible, setModalVisible] = useState(true);

  const handleLogout = async () => {
    await AsyncStorage.removeItem('token');
    navigation.navigate('Register');
  };

  return (
    <View style={styles.centeredView}>
      <Modal animationType="slide" visible={modalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modal1}>
            <Image source={Images.DONE} />
          </View>
          <View style={styles.modal2}>
            <Text style={styles.title}>
              Chúc mừng bạn đã đăng nhập thành công
            </Text>
          </View>
          <View style={styles.modal3}>
            <Text style={styles.content}>
              Tanca đưa ra các giải pháp chấm công toàn diện cho doanh nghiệp
            </Text>
          </View>
          <View style={styles.modal4}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => setModalVisible(!modalVisible)}>
              <Text style={styles.buttonText}>Đồng ý</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>

      <Pressable style={[styles.button, styles.buttonOpen]}>
        <Text style={styles.textStyle}>Đăng nhập thành công</Text>
      </Pressable>
      <View>
        <TouchableOpacity style={{backgroundColor: 'black'}}>
          <Text onPress={handleLogout} style={styles.buttonText}>
            Logout
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HomeModal;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
    backgroundColor: '#acb2c1',
    opacity: 0.8,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  buttonOpen: {
    backgroundColor: '#F194FF',
  },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  modal1: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(13),
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  modal2: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    textAlign: 'center',
    color: '#303E65',
    paddingHorizontal: 30,
  },
  modal3: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    fontSize: 17,
    fontWeight: '400',
    textAlign: 'center',
    color: '#7A8CB4',
    paddingHorizontal: 30,
  },
  modal4: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    borderBottomEndRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#1ECC78',
    borderRadius: 15,
    width: Display.setWidth(37),
    height: Display.setHeight(6),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 19,
  },
});

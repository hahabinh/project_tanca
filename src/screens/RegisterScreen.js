import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import React, {useState} from 'react';
import {Images} from '../constants';
import {Display} from '../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';

const RegisterScreen = ({navigation}) => {
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');

  const handleRegister = () => {
    if (name == '' || phone == '') {
      alert('Name and Phone Number cannot is empty');
    } else {
      navigation.navigate('Verification_Register', {name, phone});
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.top}>
        <Image source={Images.LOGO} />
      </View>
      <View style={styles.bottom}>
        <View style={styles.bottom1}>
          <View style={styles.textLogin}>
            <Text style={{color: '#303e65', fontWeight: '600', fontSize: 24}}>
              Đăng ký
            </Text>
            <Text
              style={{
                color: '#96A0BD',
                fontWeight: '400',
                fontSize: 17,
                marginTop: 10,
              }}>
              Hãy cho chúng tôi biết về bạn
            </Text>
          </View>
        </View>
        <View style={styles.input}>
          <View style={styles.input1}>
            <View style={styles.people}>
              <Icon name="people-alt" size={18} />
            </View>
            <TextInput
              style={{paddingLeft: 20}}
              placeholder="Nhập họ và tên"
              keyboardType="default"
              onChangeText={text => setName(text)}
            />
          </View>
          <View style={styles.input2}>
            <TouchableOpacity style={styles.countryContainer}>
              <Image source={Images.VIETNAM} style={styles.imgVN} />
              <Text style={styles.countryText}>+84</Text>
            </TouchableOpacity>
            <TextInput
              style={styles.phoneInputContainer}
              placeholder="Nhập số điện thoại"
              keyboardType="number-pad"
              onChangeText={text => setPhone(text)}
            />
          </View>
        </View>
        <View style={styles.bottom2}>
          <View style={styles.signinButton}>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => {
                handleRegister();
              }}>
              <Text style={styles.signinButtonText}>Đăng ký</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default RegisterScreen;

const styles = StyleSheet.create({
  people: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 13,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  top: {
    width: '100%',
    backgroundColor: '#f3fbf9',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  bottom1: {
    flex: 1,
  },
  textLogin: {
    flex: 1,
    paddingTop: 20,
  },
  input: {
    flex: 1,
  },
  input1: {
    flexDirection: 'row',
    backgroundColor: '#F2F7FF',
    borderRadius: 10,
  },
  input2: {
    marginTop: 10,
    backgroundColor: '#F2F7FF',
    flexDirection: 'row',
    borderRadius: 10,
  },
  phoneInputContainer: {
    // backgroundColor: '#F2F7FF',
    paddingLeft: 10,
  },
  countryContainer: {
    backgroundColor: '#F2F7FF',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRightWidth: 1,
    borderColor: 'black',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  countryText: {
    fontSize: 14,
    fontWeight: 700,
    color: '#303e65',
  },

  bottom2: {
    flex: 1,
  },
  imgVN: {
    width: Display.setWidth(6),
    height: Display.setHeight(3),
  },
  countryText: {
    fontSize: 14,
    fontWeight: 700,
    color: '#303e65',
  },
  signinButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    backgroundColor: '#1ECC78',
    width: Display.setWidth(90),
    height: Display.setHeight(7),
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signinButtonText: {
    color: '#fff',
    fontWeight: 500,
    fontSize: 20,
  },
  signinWith: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

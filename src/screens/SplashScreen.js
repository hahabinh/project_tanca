import {Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import React, {useEffect} from 'react';
import {Images} from '../constants';
import {Display} from '../utils';
import AsyncStorage from '@react-native-async-storage/async-storage';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {}, 2000);
  }, []);

  const getTokenLogin = async () => {
    const value = await AsyncStorage.getItem('token');
    if (value !== null) {
      navigation.navigate('Home_Modal');
      console.log('Log in is success');
    } else {
      console.log('Log in is fail ');
      navigation.navigate('Welcome');
    }
  };
  getTokenLogin();

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" translucent />
      <Image source={Images.LOGO} resizeMode="contain" style={styles.image} />
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  image: {
    height: Display.setHeight(30),
    width: Display.setWidth(60),
  },
});

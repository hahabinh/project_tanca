import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useRef} from 'react';
import {General} from '../constants';
import {WelcomeCard} from '../components';
import {Display} from '../utils';

const pageStyle = isActive =>
  isActive ? styles.page : {...styles.page, backgroundColor: '#E4ECFF'};
const Pagination = ({index}) => {
  return (
    <View style={styles.pageContainer}>
      {[...Array(General.WELCOME_CONTENTS.length).keys()].map((_, i) =>
        i === index ? (
          <View style={pageStyle(true)} key={i} />
        ) : (
          <View style={pageStyle(false)} key={i} />
        ),
      )}
    </View>
  );
};

const WelcomeScreen = ({navigation}) => {
  const [welcomeListIndex, setWelcomeListIndex] = useState(0);
  const welcomeList = useRef();

  const onViewRef = useRef(({changed}) => {
    setWelcomeListIndex(changed[0].index);
  });

  const viewConfigRef = useRef({viewAreaCoveragePercentThreshold: 50});

  const pageScroll = () => {
    welcomeList.current.scrollToIndex({
      index: welcomeListIndex < 6 ? welcomeListIndex + 1 : welcomeListIndex,
    });
  };

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" />
      <View style={styles.welcomeListContainer}>
        <FlatList
          ref={welcomeList}
          data={General.WELCOME_CONTENTS}
          keyExtractor={item => item.title}
          horizontal
          showsHorizontalScrollIndicator={false}
          pagingEnabled
          overScrollMode="never"
          viewabilityConfig={viewConfigRef.current}
          onViewableItemsChanged={onViewRef.current}
          renderItem={({item}) => <WelcomeCard {...item} />}
        />
      </View>
      <Pagination index={welcomeListIndex} />
      {welcomeListIndex === 6 ? (
        <View style={styles.buttonContainerSuccess}>
          <TouchableOpacity
            style={styles.login}
            onPress={() => navigation.navigate('Signin')}>
            <Text style={styles.loginText}>Log in</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.join}
            onPress={() => navigation.navigate('Register')}>
            <Text style={styles.joinText}>Join Now</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.buttonContainer}>
          <TouchableOpacity>
            <Text
              style={styles.buttonText}
              onPress={() => {
                welcomeList.current.scrollToEnd();
              }}>
              Skip
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              pageScroll();
            }}>
            <Text style={styles.buttonText}>Next</Text>
          </TouchableOpacity>
        </View>
      )}

      <View style={{flexDirection: 'row', fontSize: 18}}>
        <Text>Sign in with </Text>
        <Text style={{color: '#303E65'}}>Azuze AD</Text>
      </View>
    </View>
  );
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcomeListContainer: {
    height: Display.setHeight(50),
  },
  pageContainer: {
    flexDirection: 'row',
  },
  page: {
    height: 10,
    width: 10,
    // backgroundColor: '#E4ECFF',
    backgroundColor: '#1ECC78',
    borderRadius: 10,
    marginHorizontal: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: Display.setWidth(100),
    alignItems: 'center',
    padding: 80,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  button: {
    backgroundColor: '#DDDDDD',
    paddingVertical: 20,
    paddingHorizontal: 11,
    borderRadius: 32,
  },
  buttonContainerSuccess: {
    flexDirection: 'row',
    padding: 80,
  },
  login: {
    width: 160,
    height: 53,
    borderColor: '#1ECC78',
    borderWidth: 1,
    borderRadius: 14,
    marginRight: 15,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  loginText: {
    textAlign: 'center',
    color: '#1ECC78',
    fontSize: 18,
  },
  join: {
    width: 160,
    height: 53,
    borderWidth: 1,
    borderRadius: 14,
    backgroundColor: '#1ECC78',
    borderColor: '#1ECC78',
    justifyContent: 'center',
  },
  joinText: {
    textAlign: 'center',
    color: '#fff',
    fontSize: 18,
  },
});

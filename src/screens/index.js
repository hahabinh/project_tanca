import SplashScreen from './SplashScreen';
import WelcomeScreen from './WelcomeScreen';
import SigninScreen from './SigninScreen';
import VerificationScreen from './VerificationScreen';
import HomeScreen from './HomeScreen';
import RegisterScreen from './RegisterScreen';
import VerificationRegister from './VerificationRegister';
import RegisterSuccess from './RegisterSuccess';
import HomeModal from './HomeModal';

export {
  SplashScreen,
  WelcomeScreen,
  SigninScreen,
  VerificationScreen,
  HomeScreen,
  RegisterScreen,
  VerificationRegister,
  RegisterSuccess,
  HomeModal,
};

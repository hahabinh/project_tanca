import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
} from 'react-native';
import React, {useEffect, useRef, useState} from 'react';
import {Images} from '../constants';
import {Display} from '../utils';
import AsyncStorage from '@react-native-async-storage/async-storage';

const VerificationScreen = ({
  route: {
    params: {phoneNumber},
  },
  navigation,
}) => {
  //   const phoneNumberSignIn = 84339911363;
  const phoneOTPSignIn1 = 5;
  const phoneOTPSignIn2 = 6;
  const phoneOTPSignIn3 = 7;
  const phoneOTPSignIn4 = 8;
  const phoneOTPSignIn5 = 9;

  const [otp1, setOtp1] = useState('');
  const [otp2, setOtp2] = useState('');
  const [otp3, setOtp3] = useState('');
  const [otp4, setOtp4] = useState('');
  const [otp5, setOtp5] = useState('');

  const [countdown, setCountDown] = useState(60);
  const [token, setToken] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setCountDown(countdown - 1);
    }, 1000);
  }, [countdown]);

  const handleSignInOTP = async () => {
    await AsyncStorage.setItem('token', phoneNumber);
    if (
      otp1 == phoneOTPSignIn1 &&
      otp2 == phoneOTPSignIn2 &&
      otp3 == phoneOTPSignIn3 &&
      otp4 == phoneOTPSignIn4 &&
      otp5 == phoneOTPSignIn5
    ) {
      navigation.navigate('Home_Modal');
    } else {
      alert('The OTP code is wrong. Please try again');
    }
  };

  const getTokenLogin = async () => {
    const value = await AsyncStorage.getItem('token');
    if (value !== null) {
      navigation.navigate('Home_Modal');
      console.log('Log in is success');
    } else {
      console.log('Log in is fail ');
    }
  };

  getTokenLogin();

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar barStyle="dark-content" backgroundColor="#fff" translucent />
      <View style={styles.top}>
        <Image source={Images.LOGO} />
      </View>
      <View style={styles.bottom}>
        <View style={styles.bottom1}>
          <View style={styles.textLogin}>
            <Text style={{color: '#303e65', fontWeight: '600', fontSize: 24}}>
              Xác minh OTP để đăng nhập
            </Text>
            <Text
              style={{
                color: '#96A0BD',
                fontWeight: '400',
                fontSize: 17,
                marginTop: 10,
              }}>
              Nhập mã OTP gửi đến +{phoneNumber}
            </Text>
          </View>
          <View style={styles.input}>
            <View style={styles.countryContainer}>
              <TextInput
                keyboardType="phone-pad"
                style={styles.otpText}
                maxLength={1}
                onChangeText={text => setOtp1(text)}
              />
            </View>
            <View style={styles.countryContainer}>
              <TextInput
                keyboardType="phone-pad"
                style={styles.otpText}
                maxLength={1}
                onChangeText={text => setOtp2(text)}
              />
            </View>
            <View style={styles.countryContainer}>
              <TextInput
                keyboardType="phone-pad"
                style={styles.otpText}
                maxLength={1}
                onChangeText={text => setOtp3(text)}
              />
            </View>
            <View style={styles.countryContainer}>
              <TextInput
                keyboardType="phone-pad"
                style={styles.otpText}
                maxLength={1}
                onChangeText={text => setOtp4(text)}
              />
            </View>
            <View style={styles.countryContainer}>
              <TextInput
                keyboardType="phone-pad"
                style={styles.otpText}
                maxLength={1}
                onChangeText={text => setOtp5(text)}
              />
            </View>
          </View>
        </View>
        <View style={styles.bottom2}>
          <View style={styles.signinButton}>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => {
                handleSignInOTP();
              }}>
              <Text style={styles.signinButtonText}>Đồng ý</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.signinWith}>
            <View style={{flexDirection: 'row', fontSize: 18}}>
              <Text>Gửi lại sau </Text>
              <Text style={{color: '#303E65'}}>({countdown})s</Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default VerificationScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  top: {
    width: '100%',
    backgroundColor: '#f3fbf9',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  bottom1: {
    flex: 1,
  },
  textLogin: {
    flex: 1,
    paddingTop: 20,
  },
  input: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bottom2: {
    flex: 1,
  },
  countryContainer: {
    marginTop: 20,
    backgroundColor: '#f2f7ff',
    width: Display.setWidth(13.5),
    height: Display.setHeight(6),
    borderRadius: 10,
  },
  phoneInputContainer: {
    marginTop: 20,
    flexDirection: 'row',
    backgroundColor: '#f2f7ff',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    paddingLeft: 10,
    flex: 1,
    height: Display.setHeight(6),
  },
  imgVN: {
    width: Display.setWidth(6),
    height: Display.setHeight(3),
  },
  countryText: {
    fontSize: 14,
    fontWeight: 700,
    color: '#303e65',
  },
  signinButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  btn: {
    backgroundColor: '#1ECC78',
    width: Display.setWidth(90),
    height: Display.setHeight(8),
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signinButtonText: {
    color: '#fff',
    fontWeight: 500,
    fontSize: 20,
  },
  signinWith: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  otpText: {
    fontSize: 25,
    textAlign: 'center',
    paddingHorizontal: 18,
    paddingVertical: 10,
    color: '#303E65',
    fontWeight: '700',
  },
});

import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TextInput,
  StatusBar,
} from 'react-native';
import React, {useState} from 'react';
import {Images} from '../constants';
import {Display} from '../utils';
import Icon from 'react-native-vector-icons/MaterialIcons';

const phoneNumberSuccess = 84339911363;

const SigninScreen = ({navigation}) => {
  const [modalVisible, setModalVisible] = useState(false);

  const [phoneNumber, setPhoneNumber] = useState('');

  const handleSignInPhone = () => {
    if (phoneNumber == phoneNumberSuccess) {
      navigation.navigate('Verification', {phoneNumber});
    } else {
      navigation.navigate('Register');
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.top}>
        <Image source={Images.LOGO} />
      </View>
      <View style={styles.bottom}>
        <View style={styles.bottom1}>
          <View style={styles.textLogin}>
            <Text style={{color: '#303e65', fontWeight: '600', fontSize: 24}}>
              Login
            </Text>
            <Text
              style={{
                color: '#96A0BD',
                fontWeight: '400',
                fontSize: 17,
                marginTop: 10,
              }}>
              Hello, so glad you're back
            </Text>
          </View>
          <View style={styles.input}>
            <TouchableOpacity style={styles.countryContainer}>
              <Image source={Images.VIETNAM} style={styles.imgVN} />
              <Text style={styles.countryText}>+84</Text>
              <View>
                <Icon name="keyboard-arrow-down" size={18} />
              </View>
            </TouchableOpacity>
            <View style={styles.phoneInputContainer}>
              <TextInput
                placeholder="Your phone number"
                keyboardType="number-pad"
                onChangeText={text => setPhoneNumber(84 + text)}
              />
            </View>
          </View>
        </View>
        <View style={styles.bottom2}>
          <View style={styles.signinButton}>
            <TouchableOpacity
              style={styles.btn}
              onPress={() => {
                handleSignInPhone();
                // alert(phoneNumberSuccess);
                // navigation.navigate('Verification', {phoneNumber});
              }}>
              <Text style={styles.signinButtonText}>Sign in</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.signinWith}>
            <View style={{flexDirection: 'row', fontSize: 18}}>
              <Text>Sign in with </Text>
              <Text style={{color: '#303E65'}}>Azuze AD</Text>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default SigninScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  top: {
    width: '100%',
    backgroundColor: '#f3fbf9',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    flex: 1,
    width: '100%',
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  bottom1: {
    flex: 1,
  },
  textLogin: {
    flex: 1,
    paddingTop: 20,
  },
  input: {
    flex: 1,
    flexDirection: 'row',
  },
  bottom2: {
    flex: 1,
  },
  countryContainer: {
    marginTop: 20,
    backgroundColor: '#f2f7ff',
    width: Display.setWidth(22),
    height: Display.setHeight(6),
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    justifyContent: 'space-evenly',
    alignItems: 'center',
    borderRightWidth: 1,
    borderColor: '#E4ECFF',
    flexDirection: 'row',
  },
  phoneInputContainer: {
    marginTop: 20,
    flexDirection: 'row',
    backgroundColor: '#f2f7ff',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    paddingLeft: 10,
    flex: 1,
    height: Display.setHeight(6),
  },
  imgVN: {
    width: Display.setWidth(6),
    height: Display.setHeight(3),
  },
  countryText: {
    fontSize: 14,
    fontWeight: 700,
    color: '#303e65',
  },
  signinButton: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  btn: {
    backgroundColor: '#1ECC78',
    width: Display.setWidth(90),
    height: Display.setHeight(8),
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signinButtonText: {
    color: '#fff',
    fontWeight: 500,
    fontSize: 20,
  },
  signinWith: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  modal1: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(13),
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  modal2: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    textAlign: 'center',
    color: '#303E65',
    paddingHorizontal: 30,
  },
  modal3: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    fontSize: 17,
    fontWeight: '400',
    textAlign: 'center',
    color: '#7A8CB4',
    paddingHorizontal: 30,
  },
  modal4: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    borderBottomEndRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#1ECC78',
    borderRadius: 15,
    width: Display.setWidth(37),
    height: Display.setHeight(6),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 19,
  },
});

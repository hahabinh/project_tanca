import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Display} from '../utils';
import {Images} from '../constants';

const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.modal1}>
        <Image source={Images.DONE} />
      </View>
      <View style={styles.modal2}>
        <Text style={styles.title}>Chúc mừng bạn đã đăng nhập thành công</Text>
      </View>
      <View style={styles.modal3}>
        <Text style={styles.content}>
          Tanca đưa ra các giải pháp chấm công toàn diện cho doanh nghiệp
        </Text>
      </View>
      <View style={styles.modal4}>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Đồng ý</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#a8b1c0',
  },
  modal1: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(13),
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
  modal2: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    fontWeight: '600',
    textAlign: 'center',
    color: '#303E65',
    paddingHorizontal: 30,
  },
  modal3: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    fontSize: 17,
    fontWeight: '400',
    textAlign: 'center',
    color: '#7A8CB4',
    paddingHorizontal: 30,
  },
  modal4: {
    backgroundColor: '#fff',
    width: Display.setWidth(90),
    height: Display.setHeight(10),
    borderBottomEndRadius: 15,
    borderBottomLeftRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  button: {
    backgroundColor: '#1ECC78',
    borderRadius: 15,
    width: Display.setWidth(37),
    height: Display.setHeight(6),
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: '500',
    fontSize: 19,
  },
});

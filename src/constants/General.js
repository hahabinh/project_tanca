const WELCOME_CONTENTS = [
  {
    image: 'ARTWORK1',
    title: 'Chấm công qua GPS, Wifi, QR Code tích hợp sâu với AI',
  },
  {
    image: 'ARTWORK2',
    title: 'Đăng ký Ca làm, Xếp ca làm tự động',
  },
  {
    image: 'ARTWORK3',
    title: 'Giao việc, quản lý công việc quy trình và tiến độ',
  },
  {
    image: 'ARTWORK4',
    title: 'Ứng lương, nhận phiếu lương và tiền lương hàng tháng',
  },
  {
    image: 'ARTWORK5',
    title: 'Số hóa 100% giấy tờ trong doanh nghiệp',
  },
  {
    image: 'ARTWORK6',
    title: 'Quản lý các thông báo, bản tin nội bộ',
  },
  {
    image: 'ARTWORK7',
    title: 'Quản lý vị trí nhân viên trên bản đồ số',
  },
];

export default {WELCOME_CONTENTS};

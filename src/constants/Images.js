export default {
  LOGO: require('../assets/images/Logo.png'),
  ARTWORK1: require('../assets/images/Artwork1.png'),
  ARTWORK2: require('../assets/images/Artwork2.png'),
  ARTWORK3: require('../assets/images/Artwork3.png'),
  ARTWORK4: require('../assets/images/Artwork4.png'),
  ARTWORK5: require('../assets/images/Artwork5.png'),
  ARTWORK6: require('../assets/images/Artwork6.png'),
  ARTWORK7: require('../assets/images/Artwork7.png'),
  VIETNAM: require('../assets/images/VietNam.png'),
  DONE: require('../assets/images/done.png'),
};

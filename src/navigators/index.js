import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {
  SplashScreen,
  WelcomeScreen,
  SigninScreen,
  VerificationScreen,
  HomeScreen,
  RegisterScreen,
  VerificationRegister,
  RegisterSuccess,
  HomeModal,
} from '../screens';

const Stack = createStackNavigator();

const Navigators = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen name="Splash" component={SplashScreen} />
        <Stack.Screen name="Welcome" component={WelcomeScreen} />
        <Stack.Screen name="Signin" component={SigninScreen} />
        <Stack.Screen name="Verification" component={VerificationScreen} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Register" component={RegisterScreen} />
        <Stack.Screen
          name="Verification_Register"
          component={VerificationRegister}
        />
        <Stack.Screen name="Register_Success" component={RegisterSuccess} />
        <Stack.Screen name="Home_Modal" component={HomeModal} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigators;

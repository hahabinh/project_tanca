import {StyleSheet} from 'react-native';

const style = StyleSheet.create({
  item: {
    flexDirection: 'row',
    backgroundColor: 'white',
    marginBottom: 15,
    paddingVertical: 14,
    paddingHorizontal: 20,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  square: {
    width: 48,
    height: 36,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  even: {
    backgroundColor: '#53d6f2',
  },
  odd: {
    backgroundColor: '#55f253',
  },
  number: {
    fontWeight: '700',
    fontSize: 16,
    color: 'white',
  },
  content: {
    color: 'red',
    width: '80%',
    fontSize: 16,
  },
});

export default style;

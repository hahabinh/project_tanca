import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  KeyboardAvoidingView,
  Keyboard,
} from 'react-native';
import React, {useState} from 'react';
import style from './FormStyle';

const Form = props => {
  const [task, setTask] = useState('');
  handleAddTask = () => {
    if (task.length === 0) {
      alert('Bạn vui lòng nhập công việc');
      return;
    }
    props.onAddTask(task);
    setTask('');
    Keyboard.dismiss();
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={style.addTask}>
      <TextInput
        value={task}
        style={style.input}
        placeholder="Nhập công việc cần làm"
        keyboardType="default"
        onChangeText={task => setTask(task)}
      />
      <TouchableOpacity onPress={handleAddTask}>
        <View style={style.iconWrapper}>
          <Text style={style.icon}>+</Text>
        </View>
      </TouchableOpacity>
    </KeyboardAvoidingView>
  );
};

export default Form;

const styles = StyleSheet.create({});

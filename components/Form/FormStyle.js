import {StyleSheet} from 'react-native';

const style = StyleSheet.create({
  addTask: {
    bottom: 40,
    paddingHorizontal: 20,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  input: {
    height: 44,
    width: '80%',
    backgroundColor: '#fff',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#21a3d0',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  iconWrapper: {
    height: 44,
    width: 44,
    borderRadius: 44,
    backgroundColor: '#21a3d0',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#eff7f8',
  },
  icon: {
    fontSize: 24,
    color: '#fff',
  },
});

export default style;

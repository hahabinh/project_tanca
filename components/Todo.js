import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React from 'react';

const Todo = () => {
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <Text style={styles.header}>To Do List</Text>
        <ScrollView style={styles.items}>
          <TouchableOpacity>
            <View style={styles.item}>
              <View style={styles.square}>
                <Text style={styles.number}>01</Text>
              </View>
              <Text style={styles.content}>Lau nhà</Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity>
            <View style={styles.item}>
              <View style={styles.square}>
                <Text style={styles.number}>01</Text>
              </View>
              <Text style={styles.content}>Lau </Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
      </View>
      <View style={styles.input}></View>
    </View>
  );
};

export default Todo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#eff7f8',
  },
  body: {
    flex: 1,
    paddingTop: 10,
    paddingHorizontal: 20,
  },
  header: {
    fontSize: 24,
    color: '#21a3d0',
    fontWeight: 'bold',
  },
});

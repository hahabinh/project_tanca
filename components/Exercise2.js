import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Exercise2 = () => {
  return (
    <View style={styles.container}>
      <View style={styles.top}>
        <Text style={styles.topText}>Top Left</Text>
      </View>
      <View style={styles.bottom}>
        <View style={styles.bottomLeft}>
          <Text style={styles.bottomLeftText}>Bottom Left</Text>
        </View>
        <View style={styles.bottomRight}>
          <View style={styles.rightTop}>
            <Text style={styles.rightTopText}>Right Top</Text>
          </View>
          <View style={styles.rightBottom}>
            <Text style={styles.rightBottomText}>Right Bottom</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Exercise2;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    flex: 1,
    backgroundColor: '#f4fe87',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topText: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#21a3d0',
  },
  bottom: {
    flex: 1,
    flexDirection: 'row',
  },
  bottomLeft: {
    flex: 1,
    backgroundColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomLeftText: {
    fontWeight: 'bold',
    fontSize: 24,
    color: 'blue',
  },
  bottomRight: {
    flex: 1,
    backgroundColor: 'blue',
  },
  rightTop: {
    flex: 3,
    backgroundColor: 'pink',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightTopText: {
    fontWeight: 'bold',
    fontSize: 24,
    color: 'white',
  },
  rightBottom: {
    flex: 1,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightBottomText: {
    fontWeight: 'bold',
    fontSize: 24,
    color: 'white',
  },
});

import {StyleSheet, Text, View} from 'react-native';
import React from 'react';

const Exercise1 = () => {
  return (
    <View style={styles.container}>
      <View style={styles.containerTop}>
        <View style={styles.top}>
          <Text style={styles.topText}>Top Left</Text>
        </View>
      </View>
      <View style={styles.containerCenter}>
        <View style={styles.center}>
          <Text style={styles.centerText}>Center</Text>
        </View>
      </View>
      <View style={styles.containerBottom}>
        <View style={styles.bottom}>
          <Text style={styles.bottomText}>Bottom Right</Text>
        </View>
      </View>
    </View>
  );
};

export default Exercise1;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e7feff',
  },
  containerTop: {
    flex: 1,
  },
  top: {
    marginTop: 40,
    marginHorizontal: 40,
    backgroundColor: 'white',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'red',
  },
  topText: {
    fontSize: 20,
    color: '#21a3d0',
    fontWeight: 'bold',
  },
  containerCenter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    width: 150,
    height: 150,
    backgroundColor: '#33ff60',
    borderRadius: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  containerBottom: {
    flex: 1,
  },
  bottom: {
    marginTop: 160,
    marginHorizontal: 40,
    backgroundColor: '#7ee6fd',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 16,
  },
  bottomText: {
    textAlign: 'right',
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
});
